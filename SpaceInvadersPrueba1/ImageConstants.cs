using System;
using System.Collections.Generic;
using Microsoft.UI.Xaml.Media.Imaging;

namespace SpaceInvadersPrueba1;

public static class ImageConstants
{
    public static Image Shoot =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Laser.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Player =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Player.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Bunker =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Bunker.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader01_1 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_01-1.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader01_2 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_01-2.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader02_1 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_02-1.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader02_2 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_02-2.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader03_1 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_03-1.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Invader03_2 =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Invader_03-2.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Missile =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Missile.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image MysteryShip =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/MysteryShip.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Splat =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Splat.png", 30, 30, new Thickness(0, 0, 10, 0));

    public static Image Live =>
        CreateImage("ms-appx:///SpaceInvadersPrueba1/Assets/Sprites/Live.png", 30, 30, new Thickness(0, 0, 10, 0));


    private static Image CreateImage(string url, double width, double height, Thickness margin)
    {
        Image image = new Image
        {
            Source = new BitmapImage(new Uri(url)),
            Width = width,
            Height = height,
            Margin = margin
        };

        return image;
    }
}
