using Microsoft.UI.Xaml.Input;
using System;

namespace SpaceInvadersPrueba1
{
    public sealed partial class MainPage : Page
    {
        private Image shotImage;
        private DispatcherTimer timer;
        private const int BulletSpeed = 5;
        private int score = 0;
        private bool isEnemyPresent = false; 
        private Image enemyImage; 

        public MainPage()
        {
            this.InitializeComponent();
            AdjustInitialPosition();

            UpdateLives(3);
            RegisterScore(0);

            this.KeyDown += MainPage_KeyDown;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(16);
            timer.Tick += UpdateGame;
            timer.Start();
        }

        private void CanvasGame_OnPointerPressed(object sender, PointerRoutedEventArgs e)
        {
            game.Focus(FocusState.Programmatic);
        }

        private void AdjustInitialPosition()
        {
            double initialLeft = (game.ActualWidth - playerImage.ActualWidth) / 2;
            double initialTop = (game.ActualHeight - playerImage.ActualHeight);

            Canvas.SetLeft(playerImage, initialLeft);
            Canvas.SetTop(playerImage, initialTop);
        }

        private void MainPage_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            const int stepSize = 10;

            double newLeft = Canvas.GetLeft(playerImage);
            double newTop = Canvas.GetTop(playerImage);

            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            player.SoundLocation = "SpaceInvadersPrueba1/Assets/Sounds/playerSound1.wav"; 

            switch (e.Key)
            {
                case Windows.System.VirtualKey.Left:
                    newLeft = Math.Max(newLeft - stepSize, 0);
                    player.Load();
                    player.Play(); 
                    break;
                case Windows.System.VirtualKey.Right:
                    newLeft = Math.Min(newLeft + stepSize, game.ActualWidth - playerImage.ActualWidth);
                    player.Load();
                    player.Play(); 
                    break;
                case Windows.System.VirtualKey.Space:
                    Shoot();
                    break;
                case Windows.System.VirtualKey.Up:
                    Shoot();
                    break;
            }

            Canvas.SetLeft(playerImage, newLeft);
            Canvas.SetTop(playerImage, newTop);
        }

        private void UpdateLives(int quantity)
        {
            imageStackPanel.Children.Clear();
            quantity = Math.Min(quantity, 6);
            for (int i = 0; i < quantity; i++)
            {
                Image newImage = ImageConstants.Live;
                imageStackPanel.Children.Add(newImage);
            }
        }

        private void RegisterScore(int pointsEarned)
        {
            score += pointsEarned;
            scoreText.Text = score.ToString();

            int pointsForExtraLife = 500;
            int extraLives = score / pointsForExtraLife;
            UpdateLives(3 + extraLives);

            if (score == 500)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                player.SoundLocation = "SpaceInvadersPrueba1/Assets/Sounds/winSound.wav";
                player.Load();
                player.Play();
                Frame.Navigate(typeof(VictoryPage));
            }
        }

        private double GetRandomNumber(double min, double max)
        {
            Random random = new Random();
            return random.NextDouble() * (max - min) + min;
        }

        private void UpdateGame(object sender, object e)
        {
            if (enemyImage == null)
            {
                enemyImage = ImageConstants.Invader01_2;
                double randomLeft = GetRandomNumber(0, 720);
                AppearEnemy(enemyImage, randomLeft, 300);
            }

            if (shotImage != null)
            {
                double topShot = Canvas.GetTop(shotImage);
                double leftShot = Canvas.GetLeft(shotImage);

                double topEnemy = Canvas.GetTop(enemyImage);
                double leftEnemy = Canvas.GetLeft(enemyImage);

                double collisionDistance = 10; 

                if (topShot < topEnemy + enemyImage.ActualHeight &&
                    topShot + shotImage.ActualHeight > topEnemy &&
                    leftShot < leftEnemy + enemyImage.ActualWidth &&
                    leftShot + shotImage.ActualWidth > leftEnemy)
                {
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                    player.SoundLocation = "SpaceInvadersPrueba1/Assets/Sounds/killSound.wav";
                    player.Load();
                    player.Play();
                    game.Children.Remove(shotImage);
                    game.Children.Remove(enemyImage);

                    shotImage = null;
                    enemyImage = null; 

                    RegisterScore(50); 
                }
                else
                {
                    topShot -= BulletSpeed;

                    if (topShot < 0)
                    {
                        game.Children.Remove(shotImage);
                        shotImage = null;
                    }
                    else
                    {
                        Canvas.SetTop(shotImage, topShot);
                    }
                }
            }
        }

        private void Shoot()
        {
            if (shotImage == null)
            {
                shotImage = ImageConstants.Shoot;
                Canvas.SetLeft(shotImage, Canvas.GetLeft(playerImage) + (playerImage.ActualWidth - shotImage.Width) / 2);
                Canvas.SetTop(shotImage, Canvas.GetTop(playerImage));

                game.Children.Add(shotImage);

                System.Media.SoundPlayer player = new System.Media.SoundPlayer();
                player.SoundLocation = "SpaceInvadersPrueba1/Assets/Sounds/shootSound.wav"; 
                player.Load();
                player.Play();
            }
        }


        private void AppearEnemy(Image enemyImage, double left, double top)
        {
            Canvas.SetLeft(enemyImage, left);
            Canvas.SetTop(enemyImage, top);

            game.Children.Add(enemyImage);
        }
    }
}
